---
title: Avant-propos
layout: cover
---
Mémoire d'[Etienne Baron](https://urltr.ee/etxetxe) dans le cadre du Diplôme National Supérieur d'Expression Plastique, de l'[EESI Angoulême-Poitiers](https://www.eesi.eu/site/index.php), sous la direction de [Frédéric Curien](_memoire/1-a-propos/-a-propos.md) et de [Désirée Laurenz](_memoire/1-a-propos/-a-propos.md).

{{ site.version}} – {{ site.versiondate }} – [source]({{ site.repo_url }})

### Résumé
{{ site.resume }}

#### Mots-clés
{{ site.mots-cles }}

### Abstract
{{ site.abstract }}

#### Keywords
{{ site.keywords }}


### Imprimer ce site web
[Version PDF imprimable](/telechargement/baron-etienne-bifurcation-carnet-de-recherche-cc-by-nc-sa.pdf).

### Contexte
Ce travail de recherche est réalisé dans le cadre du Diplôme National Supérieur d'Expression Plastique à l'[EESI Angoulême-Poitiers](https://www.eesi.eu/site/index.php).
