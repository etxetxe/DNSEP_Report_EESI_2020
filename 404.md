---
title: Page non trouvée
permalink: /404.html
sitemap: false
---
Vous vous êtes égaré·e.

Avant de tenter de retrouver votre chemin, voici une citation de Elie Yaffa :

>J'ai demandé ma route au mur, il m'a dit d'aller tout droit.
