Cette bibliographie est également disponible sur Zotero sous forme d'un groupe, des références peuvent être ajoutées :

[https://www.zotero.org/groups/2559522/bifurcation_eesi_memoire](https://www.zotero.org/groups/2559522/bifurcation_eesi_memoire)

## Ouvrages

{% bibliography --query @book @incollection %}

## Articles

{% bibliography --query @article @inproceedings %}

<div class="break"></div>
## Thèses

{% bibliography --query @phdthesis %}

## Billets de blog, conférences, vidéos, émissions de radio, divers

{% bibliography --query @misc %}
