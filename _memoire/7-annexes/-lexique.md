Ce lexique constitue un outil pour la lecture de ce mémoire.
Pour définir les termes présentés ci-dessous plusieurs ressources ont été utilisées, dont l'encyclopédie Wikipédia, le portail lexical du Centre national de ressources textuelles et lexicales, ainsi que les ouvrages et documents présentés dans la bibliographie en annexe.
Lorsque des paraphrases ou des citations sont utilisées dans les définitions, une référence est précisée.

#### Commit
Au sein du système de gestion de versions Git, un _commit_ est un enregistrement accompagné d'un message, cela correspond à un état d'un projet géré avec Git.

#### Générateur de site statique
Un générateur de site statique, _static site generator_ ou SSG en anglais, est un programme permettant de transformer des fichiers écrits dans un format de langage de balisage léger en fichiers HTML organisés constituant un site web.
Contrairement au système de gestion de contenu classique comme le CMS Wordpress, les générateurs de site statique n'utilisent pas de bases de données.
Plus d'informations via le site de la communauté française [Jamstatic](https://jamstatic.fr/).

#### Markdown
"Inventé par John Gruber au début des années 2000, Markdown est un langage sémantique qui permet d'écrire du HTML — Hyper Text Markup Language — avec un système de balisage bien plus léger.

#### Open source
_Open source_, ou code ouvert en français, désigne les possibilités de libre redistribution, d'accès au code source et de création de travaux dérivés pour un programme ou logiciel informatique.
Si la démarche "libre" est un projet humain et politique, celle de l'open source vise avant tout l'évolution technique.

#### XML
_Extensible Markup Language_, ou langage de balisage extensible en français, est un langage informatique permettant de structurer du texte avec l'association d'une définition de type de document.

#### YAML
YAML, pour _YAML Ain't Markup Language_, est un format de représentation de données.

#### Digression
S'écarter du chemin pour y revenir

#### Improvisation
Jouer, inventer sur le fait

#### Singularité
Caractère de ce qui est singulier

#### Proxémie
Ce qui définie les relations entre chaque ramifications

#### Rhizome
Issu du vocabulaire botanique, tige souterraine à ramification structurante

#### Intertextualité
Caractère de ce qui se passe entre les lignes et les sources

#### Génération
Quand la forme elle-même crée
