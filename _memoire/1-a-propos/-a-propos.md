Ce document, que vous lisiez la version web, la version PDF ou la version imprimée, est le mémoire d'Etienne Baron sous la direction de Frédéric Curien et de Désirée Laurenz.
Il s'agit d'un travail de recherche réalisé dans le cadre du Diplôme National Supérieure d'Expression Plastique à l'[EESI Angoulême-Poitiers](https://www.eesi.eu/site/index.php).

## Soutenance
La soutenance de ce mémoire aura lieu en février 2021 à l'EESI Angoulême-Poitiers.

## Version
Version {{ site.version }}, datée du {{ site.versiondate }}.

[Version PDF imprimable](/telechargement/baron-etienne-bifurcation-carnet-de-recherche-cc-by-nc-sa.pdf).


## Licence
Licence Creative Commons BY-NC-SA, Attribution-NonCommercial-ShareAlike 4.0 International.
Ce mémoire, dans son système, est un fork du travail de thèse {% cite antoine_publi_num %} mené par [Antoine Fouchié](https://www.quaternum.net/) à l'[Enssib](http://www.enssib.fr/) sur les systèmes de publication et l'impact du numérique sur ses chaînes actuelles.
[https://memoire.quaternum.net](https://memoire.quaternum.net).

## Fabrication
Ce mémoire est fabriqué avec les langages, composants, programmes et logiciels suivants :

* langage de balisage léger [Markdown](https://commonmark.org/) ;
* générateur de site statique [Jekyll](https://jekyllrb.com/) ;
* extensions pour Jekyll : [jekyll-scholar](https://github.com/inukshuk/jekyll-scholar) et [jekyll-microtypo](https://github.com/borisschapira/jekyll-microtypo/) ;
* système de gestion de versions [Git](https://git-scm.com/) ;
* plate-forme d'hébergement de dépôt Git [GitHub](https://github.com/) ;
* déploiement continu, hébergement et CDN : [Netlify](https://www.netlify.com/) ;
* script [paged.js](https://gitlab.pagedmedia.org/tools/pagedjs) pour la conversion HTML > PDF ;
* éditeur de texte et IDE : [Atom](https://atom.io/) ;
* navigateur web : [Mozilla Firefox](https://www.mozilla.org/fr/firefox/) ;
* logiciel de gestion de références bibliographiques : [Zotero](https://www.zotero.org/).
