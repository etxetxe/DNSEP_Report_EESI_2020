>CHEMINS QUI NE MENENT NULLE PART
>
>Chemins qui ne mènent nulle part
>entre deux prés, que l’on dirait avec art
>de leur but détournés,
>
>chemins qui souvent n’ont
>devant eux rien d’autre en face
>que le pur espace
>et la saison.
>
>{% cite rilke_les_2005 %}

Je remercie Frédéric Curien et Désirée Laurenz pour avoir accepté la co-direction de ce mémoire, et pour m'avoir apporté des retours précieux, rapides, exigeants et enthousiasmants.

Merci à Thomas Leblond pour sa patience, son acharnement et sa fougue.

Merci à Julie Stæbler d'interroger le livre sous l'angle du design graphique.

Merci à Philomène Follet pour sa relecture précise.

Que soient également remerciées les précédentes promotions du Diplôme National Supérieur d'Expression Plastique qui, sans le savoir peut-être, m'ont apporté un terreau d'échanges et de réflexions inégalable.
